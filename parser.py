#!/bin/python3
import Gutenberg
from textblob import TextBlob
import pandas as pd
import unidecode 
import string
import re

def crop(text): #fonction qui va me permettre de couper le texte
    """
    function crop 
    removes the header and the footer of a Gutenberg Project book 
    ______________________________________________________________
        - input : 
            caracter string : text
        - output :
            caracter string : text 
    """
    index_debut = 0     #on initialise le début du texte
    index_fin = len(text)-1     #on intialise la fin du texte
    for i, line in enumerate(text):       #on va regarder pour chaque ligne et on compte chaque ligne entre le début et la fin pour supprimer les bonnes lignes
        if "*** START OF THE PROJECT" in line :     #on cherche ***...*** 
            index_debut = i         #on donne le début du texte à partir du ***...*** que l'on as trouvé
        if "*** END OF THE PROJECT" in line :       #on cherche ***...***
            index_fin = i       #on donne la fin du texte à partir du ***...*** que l'on as trouvé
    return text[index_debut+1:index_fin]    #on retourne ainsi le début du texte +1 pour ne pas prendre la ligne ***...*** jusqu'à la fin


def get_tittle(text):       #on veut chercher le titre
    for i, line in enumerate(text):     #on va regarder pour chaque ligne et on compte chaque ligne entre le début et ce qu'on cherche pour supprimer les bonnes lignes
        if "Title: " in line:       #on trouve Title: dans le texte 
            return line.split(': ')[1]      #on retourne la ligne ou il y a Title: mais en supprimant Title: 

def get_lang(text):     #on veut chercher la langue
    for i, line in enumerate(text):      #on va regarder pour chaque ligne et on compte chaque ligne entre le début et ce qu'on cherche pour supprimer les bonnes lignes
        if "Language: " in line:        #on trouve Language: dans le texte
            return line.split(': ')[1]      #on retourne la ligne ou il y a Language: mais en supprimant Language:

def get_author(text):       #on veut chercher l'auteur
    for i, line in enumerate(text):     #on va regarder pour chaque ligne et on compte chaque ligne entre le début et ce qu'on cherche pour supprimer les bonnes lignes
        if "Author: " in line:      #on trouve Author: dans le texte
            return line.split(': ')[1]      #on retourne la ligne ou il y a Author: mais en supprimant Author:

def get_count(text):        #on veut chercher à compter le nombre d'occurence de chaque caractère
    pattern = string.ascii_lowercase + string.digits        #on défini pattern comme une chaîne de caraactère comprenant les lettres en minuscules ainsi que les chiffres 
    tmp = pd.Series(list(text)).value_counts().to_dict()        #on défini le text en une liste qu'es elle même défini comme une série de la librairie pandas, renvoie la fréquence relative en divisant toutes les valeurs par la somme des valeurs et met en dictionnaire 
    rm = []     #c'est une nouvelle liste vide
    for key in tmp.keys():      #on va comparer toutes les clés  une à une
        if not key in pattern:      #si les caractères key n'appartiennent pas à la variable pattern
           rm.append(key)       #on ajoute chaque key à la fonction vide défini ultérieurement
    for key in rm:      #pour toutes les keys dans la liste
        del tmp[key]        #supprime toutes les keys de tmp qui ne sont pas dans pattern
    return tmp      #on renvoie tmp
    liste = []      #nouvelle liste vide
    if i in list(text) is int :     #si pour chaque caractère de la liste du texte est un entier
        sum(liste)          #on l'ajoute à la liste

def main():     #c'est la fonction principale (on appels ici les autres fonctions)
    data = Gutenberg.download_url("https://www.gutenberg.org/cache/epub/69050/pg69050.txt")      #data prend l'url de la fonction download_url dans le fichier Gutemberg
    print('"'+get_tittle(data.split('\n')) + '"')       #affiche le titre à l'aide de la fonction en redécoupant le texte en ligne et saute une ligne (ici le titre est mis entre parenthèse)
    print(get_lang(data.split('\n')))       #affiche la langue à l'aide de la fonction en redécoupant le texte en ligne et saute une ligne 
    print(get_author(data.split('\n')))     #affiche l'auteur à l'aide de la fonction en redécoupant le texte en ligne et saute une ligne 
    data = data.lower()     #le texte devient le texte mais en minuscule
    data = unidecode.unidecode(data)        #on retire les accent du texte
    data = crop(data.split('\n'))       #on utilise la fonction crop sur le texte couper en ligne
    temp = ''       #défini une liste vide
    for line in data:       #pour chaque ligne dans data
        temp+=line      #on ajoute chaque ligne à la liste temp pour en faire une seule liste
    print(get_count(temp))      #on peut alors compter l'occurence de chaque caractère de la liste temp

if __name__ == "__main__":      #permet de pas mélanger toutes les focntions main de plusieurs projet différents si rassemblement il y a, ainsi la fonction main de Gutemberg n'es pas appelé même si Gutemberg est importé dans ce projet
    main()