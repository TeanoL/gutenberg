#!/bin/python3
import requests

def download_url(args):
    print("downloading")
    url = args
    return requests.get(url).text

def write_text(text):
    with open("book.txt", "w") as fout:
        fout.write(text)

def main():
    url = "https://www.gutenberg.org/files/10/10-0.txt"    
    write_text(download_url(url))

if __name__=="__main__":
    main()